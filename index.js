
const FIRST_NAME = "Cristea";
const LAST_NAME = "Iarina Gabriela";
const GRUPA = "1075";

/**
 * Make the implementation here
 */
function numberParser(value) {
    if(value===Infinity || value===-Infinity || isNaN(value) || Number.MAX_SAFE_INTEGER<value || value<Number.MIN_SAFE_INTEGER)
        return NaN;
    return parseInt(value);
}


module.exports = {
    FIRST_NAME,
    LAST_NAME,
    GRUPA,
    numberParser
}

